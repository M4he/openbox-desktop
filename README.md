![sample screenshot](sample-screenshot.jpg)

**Table of Contents**

[[_TOC_]]

## Installation

- copy the contents of `.xsessionrc` into your `~/.xsessionrc` file
- copy all contents from `.config/` into your `~/.config/`
- copy all contents from `.themes/` into your `~/.themes/`
- make sure to install all [**Requirements**](#requirements)
- install the `usr/local/bin/intel_pstate_turbo` script if desired (see [**Under the hood**](#under-the-hood))
- read [**Things to adjust**](#things-to-adjust) and make any adjustments necessary


## Requirements

This configuration uses the following additional tools.
Most tools are used within `~/.config/openbox/autostart` unless specified otherwise.

### Essentials

- `picom` as compositor for vsync and slightly rounded window corners
    - note: I've been using version [vgit-3680d](https://github.com/yshui/picom/tree/3680d323f5edf2ef5f21ab70a272358708c87a22), any recent versions may discolor Openbox borders!
- `nitrogen` as wallpaper setter
- `tint2` as panel and desktop icon bar
- `wmctrl`, `xdotool` for the desktop switching button on the tint2 panel
- `gsimplecal` for the calendar popup on the panel clock
- `jgmenu`\* used as the start menu
- `xfce4-notifyd` for notification popups (and `systemctl` for starting it, see `autostart`)
- `xsettingsd` and `qt5ct` to enforce UI and icon themes in GTK and Qt respectively
- `python3` for some of the included scripts
    - plus `python3-psutil` for the `scripts/exit.py` logout script
- `zenity` and `notify-send` (`notify-send` is often part of `libnotify-bin`) for the logout prompt in `scripts/exit.sh`
- `xmlstarlet` for the automated Openbox XML edit in `scripts/switch-theme.sh`
- `mpv` for playing the login and logout sounds (used in `autostart`and `exit.py`)
- the font **Roboto** and **Roboto Condensed** for UI
- the font [**Iosevka Curly 6.1.3**](https://github.com/be5invis/Iosevka/releases/tag/v6.1.3)\*\* for the conky config
- the font **Quicksand** for parts of the Openbox theme

\* This config supports local jgmenu built with `--prefix=$HOME/.local` (see the `export PATH` in `.config/openbox/environment`) as per [jgmenu instructions](https://github.com/johanmalm/jgmenu/blob/master/INSTALL.md#build-and-install-in-home-directory) so that `jgmenu` is at `~/.local/bin/jgmenu`.
For building jgmenu [from source](https://github.com/johanmalm/jgmenu) on a Debian-based system, you need: `pkg-config libxml2-dev libx11-dev libxrandr-dev libcairo-dev libpango1.0-dev librsvg2-dev libmenu-cache-dev`.

\*\* A specific Iosevka version is linked here because more recent versions will have different render scaling in conky and won't match the conky config provided here.


### Optionals

- `numlockx` for initializing numlock state at startup
- `nm-applet` for network tray icon
- `mate-volume-control-status-icon` as volume tray icon (from `mate-media` package)
- `system-config-printer-applet` for print queue tray icon
- `xpad` for sticky notes with tray icon
- `conky` for system monitoring

### Recommendations

- `gtk3-nocsd` to get rid of Gnome3 client-side decorations and use Openbox's window borders instead
- [Win1X icon theme](https://gitlab.com/M4he/Win1x-icon-theme)
    - best to install this as root in `/usr/share/icons/` for `jgmenu` to pick it up correctly
- [Stylish GTK theme (with 'Blue' patch)](https://gitlab.com/M4he/stylish-gtk-theme)
- [Breeze Snow cursor theme](https://code.jpope.org/jpope/breeze_cursor_sources/src/master/breeze-snow-cursor-theme.zip)
- Breeze Qt theme


## Contents of `.config/openbox/`

| File | Description |
|---|---|
| `autostart` | Autostart instructions for Openbox, uses a lot of external tools. See [**Requirements**](#requirements). |
| `environment` | Specifies environment variables and initialization instructions for Openbox. Note [**KDE/Qt style linking mechanism**](#kdeqt-style-linking-mechanism) below! |
| `menu.xml` | Openbox menu definition. |
| `rc.xml` | Main Openbox configuration. |
| `kdeglobals` | KDE apps color configuration specific to Openbox, see [**KDE/Qt style linking mechanism**](#kdeqt-style-linking-mechanism) below. |
| `picom.conf` | Configuration for the `picom` compositor. |
| `scripts/switch-theme.sh` | Script for switching between light and dark theme presets for GTK, icons, tint2 and Openbox. |
| `scripts/toggle-center.py` | A Python3 script for displaying a GTK3 popup window with toggles for custom on/off settings. |
| `scripts/exit.py` | A sophisticated Python3 script for gracefully terminating applications before exiting Openbox. |
| `scripts/exit.sh` | Shows a Yes/No logout prompt before calling `exit.py`. |

| Folder | Description |
|---|---|
| `icons/` | Icon sets for the Openbox menu, used in `menu.xml` |
| `conky/` | Conky skins to be used with this config. |
| `nitrogen/` | Configuration of `nitrogen`, relocated to be specific to Openbox. |
| `sounds/` | Login and logout chimes. |
| `scripts/` | Various scripts, see above. |
| `qt5ct/` | Qt5ct configuration specific to Openbox, see [**KDE/Qt style linking mechanism**](#kdeqt-style-linking-mechanism) below. |


## Things to adjust

General:

- search and replace "nautilus", "xfce4-terminal", "subl", "audacious" etc. throughout the config files to change them to your preferred applications instead
- configure application shortcuts in `~/.config/tint2/starter/` (panel starters) and `~/.config/tint2/desktop/` (desktop icon bar)

In `~/.config/openbox/`:

- adjust `autostart`
- adjust the path to the home directory for icons in `menu.xml`
- edit `autostart` and `scripts/exit.py` to change the startup/shutdown sounds
- edit `rc.xml` to modify application keybindings
- for GTK2/3 styling, edit `.xsettingsd`
- for Qt styling, execute `qt5ct` to modify the settings
- if your `jgmenu` executable location differs, adjust `rc.xml` (keybindings) and the `~/.config/tint2/*.tint2rc` files (start button)
- the `TOGGLES` struct in `scripts/toggle-center.py`
- set monitor names and resolutions in `scripts/xrandr.sh`

### GTK, Qt, icon and cursor theme settings

This setup does rely solely on configuration files (except for `qt5ct`) for setting up theme and icon configuration for GTK and Qt:

- GTK2/3 theme, icons, cursors: `xsettingsd` service and an appropriate `~/.config/openbox/.xsettingsd` file
- Qt5 theme, icons: `qt5ct` + `QT_QPA_PLATFORMTHEME=qt5ct` together with `~/.config/kdeglobals` and `~/.config/qt5ct/`
- X cursors (outside GTK/Qt): `xrdb -merge` on `~/.config/openbox/.Xdefaults` in `~/.xsessionrc`

### Wallpaper

This config uses `nitrogen` for setting up the wallpaper *but* it redirects nitrogen's configuration file path into the `~/.config/openbox/nitrogen/` folder in order for Openbox to have its dedicated wallpaper configuration and co-exist with other WMs also using nitrogen without interfering.

You may use the included `.config/set-wallpaper.sh` for changing the wallpaper or do it via nitrogen's GUI while redirecting the config path:

```
XDG_CONFIG_HOME=$HOME/.config/openbox nitrogen --force-setter=xinerama
```


## Under the hood

### General

- some applications (such as browser, mail client etc.) have a desktop affinity set in `.config/openbox/rc.xml` and will always be moved to virtual desktop number 2
- `usr/local/bin/intel_pstate_turbo` is a helper script allowing easy on/off switching of Intel Turbo Boost for Intel CPUs, place it into `/usr/local/bin/`
    - usually needs root privileges, to make it accessible to normal users via `sudo` without password prompt: add the following to your `sudoers` file:
        ```
        ALL ALL = (root) NOPASSWD: /usr/local/bin/intel_pstate_turbo
        ```
    - the `.config/openbox/scripts/toggle-center.py` depends on that script for the Turbo Boost toggle
- jgmenu is set to `stay_alive = 0` and will spawn anew any time the start menu is opened, this is to support multiple screens, because it would otherwise stick to the screen where first opened
    - on a single screen setup you may configure `stay_alive = 1` in `.config/jgmenu/jgmenurc` to keep it running in the background and speed up opening the menu on key/button press

### tint2 config linking

- the tint2 configs `.config/tint2/tint2rc` and `.config/tint2/desktop.tint2rc` loaded in `autostart` are symbolic links
- depending on the theme, they point to the corresponding file:
    - light mode:
        ```
        desktop.tint2rc -> desktop-light.tint2rc
        tint2rc -> Win1X.tint2rc
        ```
    - dark mode:
        ```
        desktop.tint2rc -> desktop-dark.tint2rc
        tint2rc -> Win1X-dark.tint2rc
        ```
- this allows `.config/openbox/scripts/switch-theme.sh` switching between light and dark mode easily

### KDE/Qt style linking mechanism

This configuration will redirect `~/.config/{qt5ct/,kdeglobals}` to another location using symlinks.
The `environment` file in this configuration will make the following symlinks when Openbox starts:

```
~/.config/qt5ct -> ~/.config/openbox/qt5ct
~/.config/kdeglobals -> ~/.config/openbox/kdeglobals
```

This will lead to dedicated Qt styling config being stored in `~/.config/openbox`.
When using other DEs or WMs, those can apply their own symlinks to different targets at startup to separate their Qt styling from this Openbox config.

For example, you can extend your `~/.xessionrc` like this:

```bash
if [ "$DESKTOP_SESSION" = "openbox" ]
then
    xrdb -merge $HOME/.config/openbox/.Xdefaults
elif [ "$DESKTOP_SESSION" = "plasma" ]
then
    mkdir -p $HOME/.config/qt5ct.plasma
    ln -sfn $HOME/.config/qt5ct.plasma $HOME/.config/qt5ct
    touch $HOME/.config/kdeglobals.plasma
    ln -sfn $HOME/.config/kdeglobals.plasma $HOME/.config/kdeglobals
    xrdb -merge $HOME/.Xdefaults
    export QT_QPA_PLATFORMTHEME=qt5ct
fi
```

... which will redirect the configs to `~/.config/qt5ct.plasma/` and `~/.config/kdeglobals.plasma` respectively instead when KDE/Plasma starts.


## Keybindings

(optimized for QWERTZ keyboards)

### Global shortcuts

- `[Super]` (windows key): open start menu (jgmenu)
- `[Alt]` + `[Enter]`: open terminal
- `[Alt]` + `[Shift]` + `[Enter]`: open file manager
- `[Ctrl]` + `[Alt]` + `[Del]`: open task manager
- `[Ctrl]` + `[Shift]` + `[Esc]`: open task manager
- `[Alt]` + `[V]`: open toggle center
    - the toggle center itself also has keyboard navigation: `[Up]`, `[Down]` for selection, `[Enter]` or `[Space]` for toggling and `[Esc]` to close

### Window and Desktop management

- `[Alt]` + `{[W], [A], [S], [D]}`: switch window by direction
- `[Alt]` + `[Shift]` + `{[W], [A], [S], [D]}`: tile window to one screen edge by direction
- `[Alt]` + `[Shift]` + `[E]`: tile window to screen center
- `[Alt]` + `[Space]`: Openbox window menu
- `[Alt]` + `[M]`: maximize/unmaximize window
- `[Alt]` + `[Y]`: minimize window
- `[Alt]` + `[Q]`: close window
- `[Alt]` + `[Tab]`: interactive window switcher
- `[Alt]` + `[Shift]` + `[Tab]`: interactive window switcher (reverse)
- `[Alt]` + `[^]`: non-interactive A-B window switch
- `[Alt]` + `[Esc]`: interactive desktop switcher
- `[Alt]` + `{[1] .. [5]}`: switch desktop by number
- `[Ctrl]` + `[Alt]` + `{[Left], [Right]}`: switch desktop by direction
- `[Ctrl]` + `[Shift]` + `[Alt]` + `{[Left], [Right]}`: switch desktop and move active window

### Key chains

- `[Alt]` + `[C]`, then `[M]`, then `[M]`: set external screen to mirror
- `[Alt]` + `[C]`, then `[M]`, then `[E]`: set external screen to extend to right
- `[Alt]` + `[C]`, then `[M]`, then `[P]`: switch to primary screen (turn off external)
- `[Alt]` + `[C]`, then `[M]`, then `[S]`: switch to external screen (turn off primary)

There might be more keybindings, look at `rc.xml`.

### Others

- desktop switching button on the tint2 panel
    - left click: next desktop
    - right click: previous desktop
    - middle click: A-B switch desktop
- audacious button on the tint2 panel (MPRIS player control)
    - left click: pause/play
    - right click: next track
    - middle click: previous track
    - mouse wheel: volume up/down

## Credits

Included wallpaper [by Brady Bellini from Unsplash](https://unsplash.com/photos/_hpk_92Crhs).