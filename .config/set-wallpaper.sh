#!/bin/bash
#
#
# $1 = path to image
# $2 = 2 if secondary screen (optional)
#
if [ "$XDG_SESSION_DESKTOP" == "openbox" ]; then
    # use a specific configuration file path for openbox
    if [ "$2" == "2" ]; then
        XDG_CONFIG_HOME=$HOME/.config/openbox nitrogen --force-setter=xinerama --save --head=1 --set-zoom-fill "$1"
    else
        XDG_CONFIG_HOME=$HOME/.config/openbox nitrogen --force-setter=xinerama --save --head=0 --set-zoom-fill "$1"
    fi
else
    # use the default configuration file path for others
    if [ "$2" == "2" ]; then
        nitrogen --force-setter=xinerama --save --head=1 --set-zoom-fill "$1"
    else
        nitrogen --force-setter=xinerama --save --head=0 --set-zoom-fill "$1"
    fi
fi
exit 0
