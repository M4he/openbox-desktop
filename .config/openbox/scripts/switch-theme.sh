#!/bin/bash

_change_openbox_theme () {
    xmlstarlet ed -L -N a="http://openbox.org/3.4/rc" \
    -u /a:openbox_config/a:theme/a:name \
    -v "$1" $HOME/.config/openbox/rc.xml
}

dark_theme () {
    pushd $HOME/.config/tint2 > /dev/null
    ln -sf desktop-dark.tint2rc desktop.tint2rc
    ln -sf Win1X-dark.tint2rc tint2rc
    popd > /dev/null
    _change_openbox_theme "Openbox-Lisk"

    sed -i 's@Net/IconThemeName.*@Net/IconThemeName "Win1X-dark"@' $HOME/.config/openbox/.xsettingsd
    sed -i 's@Net/ThemeName.*@Net/ThemeName "Stylish-Dark-Laptop"@' $HOME/.config/openbox/.xsettingsd
    killall -HUP xsettingsd
    killall -SIGUSR1 tint2
    openbox --reconfigure
}

light_theme () {
    pushd $HOME/.config/tint2 > /dev/null
    ln -sf desktop-light.tint2rc desktop.tint2rc
    ln -sf Win1X.tint2rc tint2rc
    popd
    _change_openbox_theme "Openbox-Blue"

    sed -i 's@Net/IconThemeName.*@Net/IconThemeName "Win1X"@' $HOME/.config/openbox/.xsettingsd
    sed -i 's@Net/ThemeName.*@Net/ThemeName "Stylish-Light-Laptop-Blue"@' $HOME/.config/openbox/.xsettingsd
    killall -HUP xsettingsd
    killall -SIGUSR1 tint2
    openbox --reconfigure
}


if [[ $# -ne 1 ]]; then
    echo 'Too many/few arguments, expecting one' >&2
    exit 1
fi

case $1 in
    light)
        light_theme
        ;;
    dark)
        dark_theme
        ;;
    check_dark_mode)
        grep -q "ThemeName \"Stylish-Dark" $HOME/.config/openbox/.xsettingsd && echo "on" || echo "off"
        ;;
    toggle)
        grep "Win1X-dark" $HOME/.config/openbox/.xsettingsd > /dev/null
        if [ "$?" -eq "0" ]; then
            light_theme
        else
            dark_theme
        fi
        ;;
    *)
        echo 'Expected "light", "dark" or "toggle"' >&2
        exit 1
esac