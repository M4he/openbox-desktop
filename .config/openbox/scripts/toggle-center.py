#!/usr/bin/env python3

import os
import gi
import signal
import subprocess

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, Gdk


TOGGLES = [
    {
        "icon": "redshift",
        "label": "Redshift Light Filter",
        "check": "bash -c '(pgrep redshift > /dev/null && echo on) || echo off'",
        "on": "redshift &",
        "off": "killall redshift"
    },
    {
        "icon": "theme-config",
        "label": "Dark theme",
        "check": "$HOME/.config/openbox/scripts/switch-theme.sh check_dark_mode",
        "on": "$HOME/.config/openbox/scripts/switch-theme.sh dark",
        "off": "$HOME/.config/openbox/scripts/switch-theme.sh light"
    },
    {
        "icon": "preferences-desktop-sound",
        "label": "PulseAudio input loopback",
        "check": "$HOME/.config/openbox/scripts/pa-loopback.sh check",
        "on": "$HOME/.config/openbox/scripts/pa-loopback.sh on",
        "off": "$HOME/.config/openbox/scripts/pa-loopback.sh off"
    },
    {
        "icon": "cpu",
        "label": "Intel Turbo Boost",
        "check": "intel_pstate_turbo check",
        "on": "sudo intel_pstate_turbo on",
        "off": "sudo intel_pstate_turbo off"
    },

]
TIMER_TICK_MS = 1000

CSS_STYLE = b"""
/* window { background-color: transparent; } */
/* .window-box { background-color: inherit; } */
.toggle-box { padding: 10px 15px 10px 15px; }
.toggle-box:hover { background-color: rgba(160, 160, 160, 0.25);
                    background-clip: padding-box; }
.toggle-box:not(:hover) { background-color: inherit; }
"""


PID_LOCK_FILE = "/tmp/togglecenter.pid"

gset = Gtk.Settings.get_default()

def _get_shell_command_output(cmd):
    return subprocess.check_output(cmd, shell=True).decode().strip()


def _exec_shell_command(cmd):
    subprocess.run(cmd, shell=True)


def _acquire_pid_lock():
    if os.path.exists(PID_LOCK_FILE):
        with open(PID_LOCK_FILE, 'r') as pid_file:
            try:
                pid = int(pid_file.read())
                os.kill(pid, signal.SIGTERM)
            except:
                pass
        _release_pid_lock()
        return False
    else:
        with open(PID_LOCK_FILE, 'w') as pid_file:
            pid_file.write(str(os.getpid()))
        return True


def _release_pid_lock():
    try:
        os.remove(PID_LOCK_FILE)
    except:
        pass


class ToggleLine(Gtk.EventBox):

    def __init__(self, root, data):
        super().__init__()
        self.parent = root
        self.set_above_child(True)

        self.get_style_context().add_class('toggle-event-box')

        box = self.box = Gtk.Box(spacing=20)
        self.set_size_request(300, -1)
        self.set_can_focus(True)

        self.add_events(Gdk.EventMask.BUTTON_PRESS_MASK)
        self.add_events(Gdk.EventMask.BUTTON_RELEASE_MASK)
        self.add_events(Gdk.EventMask.KEY_PRESS_MASK)
        self.add_events(Gdk.EventMask.ENTER_NOTIFY_MASK)
        self.add_events(Gdk.EventMask.LEAVE_NOTIFY_MASK)

        box.get_style_context().add_class('toggle-box')

        self.data = data
        t_icon = Gtk.Image.new_from_icon_name(data['icon'], Gtk.IconSize.DIALOG)
        t_icon.set_pixel_size(32)
        box.pack_start(t_icon, False, False, 0)

        t_label = Gtk.Label(label=data['label'])
        t_label.set_selectable(False)
        t_label.set_justify(Gtk.Justification.LEFT)
        t_label.set_xalign(0.0)
        box.pack_start(t_label, True, True, 0)

        self.switch = Gtk.Switch()
        t_switch_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        t_switch_box.pack_start(self.switch, True, False, 0)
        self.switch.set_can_focus(False)
        box.pack_end(t_switch_box, False, False, 0)

        self.connect('button-release-event', self.cb)
        self.connect('key-press-event', self.on_key_press)
        self.connect('enter-notify-event', self.cb_enterleave, True)
        self.connect('leave-notify-event', self.cb_enterleave, False)
        self.connect('focus-in-event', self.cb_enterleave, True)
        self.connect('focus-out-event', self.cb_enterleave, False)
        self.add(box)
        self.update_switch()

    def cb(self, *args, **kwargs):
        target = "off" if self.switch.get_active() else "on"
        print("%s: -> %s" % (self.data['label'], target))
        _exec_shell_command(self.data[target])
        self.update_switch()

    def on_key_press(self, widget, data):
        key = Gdk.keyval_name(data.keyval)
        if key == "space":
            # on space execute the toggle action
            self.cb()
            return True  # stop other handlers from handling it
        if key == "Return":
            # on Enter execute the toggle action
            self.cb()
            return True  # stop other handlers from handling it
        if key == "Escape":
            # on Esc quit the application
            self.parent.on_destroy()
        return False

    def cb_enterleave(self, event, data, is_active):
        if is_active:
            self.box.get_style_context().set_state(Gtk.StateFlags.PRELIGHT)
            self.grab_focus()
        else:
            self.box.get_style_context().set_state(Gtk.StateFlags.NORMAL)

    def get_state(self):
        return _get_shell_command_output(self.data['check'])

    def update_switch(self):
        state = self.get_state()
        self.switch.set_state((state == "on"))

    def on_switch_activated(self, switch, gparam):
        target = "on" if switch.get_active() else "off"
        state = self.get_state()
        if target != state:
            print("%s: -> %s" % (self.data['label'], target))
            _exec_shell_command(self.data[target])


class ToggleWindow(Gtk.Window):

    def __init__(self):
        super().__init__(title="Configuration Center")
        self.set_icon_name('preferences-system')
        self.set_type_hint(Gdk.WindowTypeHint.UTILITY)
        self.set_resizable(False)
        self.set_keep_above(True)
        self.set_skip_taskbar_hint(True)
        self.set_skip_pager_hint(True)
        self.set_name("ToggleCenter")  # enables '#ToggleCenter' CSS selector
        self.set_wmclass("ToggleCenter", "ToggleCenter")

        carrier = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        carrier.get_style_context().add_class('window-box')

        self.toggles = []
        for toggle_data in TOGGLES:
            toggle = ToggleLine(self, toggle_data)
            self.toggles.append(toggle)
            carrier.pack_start(toggle, True, True, 0)
        self.add(carrier)

        self.timeout_id = None
        self.start_update_timer()
        self.connect("map-event", self.on_show)
        self.connect("unmap-event", self.on_hide)
        self.connect("destroy", self.on_destroy)
        # self.connect("focus-out-event", self.on_destroy)

        css_provider = Gtk.CssProvider()
        css_provider.load_from_data(CSS_STYLE)
        context = Gtk.StyleContext()
        screen = Gdk.Screen.get_default()
        context.add_provider_for_screen(screen, css_provider,
                                        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    def on_show(self, *args, **kwargs):
        self.start_update_timer(immediate=True)

    def on_hide(self, *args, **kwargs):
        self.stop_update_timer()

    def start_update_timer(self, immediate=False):
        if self.timeout_id:
            print("Timer already registered, not starting timer!")
            return
        timeout = TIMER_TICK_MS if not immediate else 10
        self.timeout_id = GLib.timeout_add(timeout, self.on_timeout, None)

    def stop_update_timer(self):
        if self.timeout_id:
            GLib.source_remove(self.timeout_id)
            self.timeout_id = None

    def on_timeout(self, *args, **kwargs):
        self.stop_update_timer()  # cleanup references
        for toggle in self.toggles:
            toggle.update_switch()
        self.start_update_timer()

    def on_destroy(self, *args):
        self.stop_update_timer()
        Gtk.main_quit()
        _release_pid_lock()


if __name__ == "__main__":
    if _acquire_pid_lock():
        win = ToggleWindow()
        win.show_all()
        win.grab_focus()
        Gtk.main()
