#!/usr/bin/env python3

""" openbox graceful logout script

This script will iterate through the list of running processes
and attempt to terminate (SIGTERM) all user processes which
most likely belong to the desktop session¹ gracefully and
finally exit openbox itself.

¹ = processes that aren't owned by the local systemd process
    or other users
"""
import os
import subprocess
import psutil
import time
from datetime import datetime

# time in seconds after which openbox is quit at the latest
# after other processes have been attempted to be terminated
TERMINATE_TIMEOUT_SECONDS = 3

ENABLE_LOGOUT_SOUND = True
LOGOUT_SOUND = os.path.join(
    os.path.expanduser('~'),
    '.config/openbox/sounds/KDE_Logout_1.ogg')


def find_systemd_user_proc():
    print('finding systemd --user process...')
    for proc in psutil.process_iter():
        if not proc.cmdline() or len(proc.cmdline()) < 2:
            continue
        path = proc.cmdline()[0]
        arg1 = proc.cmdline()[1]
        if 'systemd' in path and '--user' == arg1:
            print('systemd process found')
            return proc

    return None


def find_proc_per_name(process_name):
    print('finding process for %s...' % process_name)
    for proc in psutil.process_iter():
        try:
            pname = proc.name()
            if pname == process_name:
                print("found process %s" % proc.pid)
                return proc
        except (psutil.NoSuchProcess,
                psutil.AccessDenied,
                psutil.ZombieProcess):
            pass
    return None


def play_logout_sound_and_return_pid():
    p = subprocess.Popen(
        'mpv -vo null --loop-file=no --volume=50 %s' % LOGOUT_SOUND,
        shell=True
    )
    print('logout sound pid is %s' % str(p.pid))
    return p.pid


print('--- %s ---' % str(datetime.now()))
print('executing graceful logout...')
# we will attempt to terminate all processes except
# for those that are part of an ignore list
pid_ignorelist = []

# do not terminate processes controlled by local systemd
systemd_proc = find_systemd_user_proc()
for proc in systemd_proc.children(recursive=True):
    pid_ignorelist.append(proc.pid)
# do not terminate the systemd process itself
pid_ignorelist.append(systemd_proc.pid)
print('added systemd childs to ignore')

# do not terminate the session yet
openbox_proc = find_proc_per_name('openbox')
pid_ignorelist.append(openbox_proc.pid)
print('added openbox to ignore')

# do not terminate the process running this script
self_process = psutil.Process()
pid_ignorelist.append(self_process.pid)
print('added self to ignore')

# do not terminate any processes and its childs of CMD_SKIPLIST
CMD_SKIPLIST = ['tmux', 'screen']
for proc in psutil.process_iter():
    if not proc.cmdline():
        continue
    path = proc.cmdline()[0]
    for cmd in CMD_SKIPLIST:
        if path.lower().endswith(cmd):
            for cmd_child in proc.children(recursive=True):
                pid_ignorelist.append(cmd_child.pid)
            pid_ignorelist.append(proc.pid)
            print('added skiplist process tree for %s to ignore' % cmd)

user = os.environ['USER']
wait_list = []
for proc in psutil.process_iter():

    # processes might vanish at any time so we wrap this into try-except
    try:
        # skip ignored processes
        if proc.pid in pid_ignorelist:
            print('skipping ignored process %s' % str(proc.pid))
            continue

        # skip processes of other users
        if proc.username() != user:
            print('skipping other user\'s process %s' % str(proc.pid))
            continue

        # skip the process if it is any parent of this script
        is_self_terminator = False
        for child in proc.children(recursive=True):
            if child.pid == self_process.pid:
                is_self_terminator = True
                break

        # actually terminate the process
        if not is_self_terminator:
            print('attempting to terminate process %s' % str(proc.pid))
            proc.terminate()
            # if we actually terminated it, add it to the wait list
            wait_list.append(proc)
        else:
            print('skipping self parent process %s' % str(proc.pid))
            continue

    except (psutil.NoSuchProcess,
            psutil.AccessDenied,
            psutil.ZombieProcess):
        print('process %s vanished or became inaccessible' % str(proc.pid))
        continue

if ENABLE_LOGOUT_SOUND:
    # play logout sound and wait for it
    logout_sound_pid = play_logout_sound_and_return_pid()
    print('waiting for logout sound to finish')
    if psutil.pid_exists(logout_sound_pid):
        psutil.wait_procs([psutil.Process(logout_sound_pid)], timeout=5)

# wait for all processes to terminate or timeout to trigger
gone, alive = psutil.wait_procs(wait_list, timeout=TERMINATE_TIMEOUT_SECONDS)
print('waited for processes, gone: %s, alive: %s' % (
    str(len(gone)), str(len(alive))))

# finally, end the session by terminating the window manager
print('attempting openbox exit...')
os.popen("openbox --exit")

# if it still hasn't quit, kill it with fire
time.sleep(1)
print('killing openbox')
if psutil.pid_exists(openbox_proc.pid):
    openbox_proc.kill()
