#!/bin/bash

if zenity --question \
    --width=320 --timeout=10 \
    --title="Openbox Logout" \
    --text="\nClose all open programs and log out?"
then
    $HOME/.config/openbox/scripts/exit.py &> $HOME/.openbox-exit.log
else
    notify-send -i "dialog-warning" "Openbox Logout" "Logout aborted"
fi